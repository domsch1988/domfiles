#!/bin/sh
xrandr --output eDP --off --output HDMI-A-0 --primary --mode 2560x1440 --pos 1440x1100 --rotate normal --output DisplayPort-0 --off --output DisplayPort-1 --off --output DisplayPort-2 --mode 2560x1440 --pos 0x0 --rotate right --output DisplayPort-3 --off
