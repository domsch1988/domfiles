#!/bin/sh
xrandr --output eDP --off --output HDMI-A-0 --primary --mode 2560x1440 --dpi 96 --pos 1440x816 --rotate normal --output DisplayPort-0 --mode 2560x1440 --dpi 96 --pos 0x0 --rotate left --output DisplayPort-1 --off
