#!/bin/bash
# Symbols
pl_left=
pl_right=

col_bg='#222436'
col_ac1='#82aaff'
col_ac2='#c3e88d'
col_ac3='#c099ff'
col_fail='#ff757f'

# Clock
date=$(date +'%Y-%m-%d %H:%M %p')

# System Monitor
memory="  $(free -m | grep Speicher | awk '{printf "%.0f", ($3/$2)*100}')%"

# Current Layout
layoutdata=$(i3-msg -t get_tree | jq -r 'recurse(.nodes[];.nodes!=null)|select(.nodes[].focused).layout')
if [ "$layoutdata" = "splith" ];
then
    layout=" "
else
    layout="󰓩 "
fi
layoutformat="$layout"

# Mounts
mountq=$(mount | grep /mnt/smb/q)
mounth=$(mount | grep /mnt/smb/h)
mounttemp=$(mount | grep /mnt/smb/temp)
mountproj=$(mount | grep /mnt/smb/projekte)
mountent=$(mount | grep /mnt/smb/entwicklung)
mountdaten=$(mount | grep /mnt/smb/elara-daten)

if [ -n "$mountq" ];
then
    mountstring=" "
else
    mountstring="${mountstring} Q   "
fi

if [ -n "$mounth" ];
then
    mountstring=" "
else
    mountstring="${mountstring}  H   "
fi

if [ -n "$mounttemp" ];
then
    mountstring=" "
else
    mountstring="${mountstring}  Temp   "
fi

if [ -n "$mountproj" ];
then
    mountstring=" "
else
    mountstring="${mountstring}  Proj   "
fi

if [ -n "$mountent" ];
then
    mountstring=" "
else
    mountstring="${mountstring}  Entw   "
fi

if [ -n "$mountdaten" ];
then
    mountstring=" "
else
    mountstring="${mountstring}  Daten   "
fi

mousbat="$(mousebattery)"

# Volume
volume="  $(pactl get-sink-volume 0 | grep % | awk '{print $5}')"

echo "$layoutformat  $mousbat   ${mountstring}"
