#!/bin/sh
# Symbols
pl_left=
pl_right=

col_bg='#222436'
col_ac1='#82aaff'
col_ac2='#c3e88d'
col_ac3='#c099ff'
col_fail='#ff757f'

# Clock
date=$(date +'%Y-%m-%d %H:%M %p')

# System Monitor
memory="󰍛  $(free -m | grep Speicher | awk '{printf "%.0f", ($3/$2)*100}')%"

# VPN Status
vpn=$(nmcli | grep dosa)
if [ -n $vpn ]
then
    vpnstatus="󰒄 OFF"
else
    vpnstatus="󰒄 ON"
fi

# Current Layout
layoutdata=$(swaymsg -t get_workspaces -p | grep focused -A 4 | grep Representation | awk '{print $2}' | grep -E 'H\[T')
if [ -n $layoutdata ]
then
    layout="Split"
else
    layout="Tabbed"
fi
layoutformat="$layout"

# Mounts
mountq=$(mount | grep /mnt/smb/q)
mounth=$(mount | grep /mnt/smb/h)
mounttemp=$(mount | grep /mnt/smb/temp)
mountproj=$(mount | grep /mnt/smb/projekte)
mountent=$(mount | grep /mnt/smb/entwicklung)
mountdaten=$(mount | grep /mnt/smb/elara-daten)

if [ -n $mountq ]
then
    mountstring="${mountstring}<span foreground='$col_fail'> </span>Q   "
else
    mountstring="<span foreground='$col_ac2'> </span>"
fi

if [ -n $mounth ]
then
    mountstring="${mountstring}<span foreground='$col_fail'> </span> H   "
else
    mountstring="<span foreground='$col_ac2'> </span>"
fi

if [ -n $mounttemp ]
then
    mountstring="${mountstring}<span foreground='$col_fail'> </span> Temp   "
else
    mountstring="<span foreground='$col_ac2'> </span>"
fi

if [ -n $mountproj ]
then
    mountstring="${mountstring}<span foreground='$col_fail'> </span> Proj   "
else
    mountstring="<span foreground='$col_ac2'> </span>"
fi

if [ -n $mountent ]
then
    mountstring="${mountstring}<span foreground='$col_fail'> </span> Entw   "
else
    mountstring="<span foreground='$col_ac2'> </span>"
fi

if [ -n $mountdaten ]
then
    mountstring="${mountstring}<span foreground='$col_fail'> </span> Daten   "
else
    mountstring="<span foreground='$col_ac2'> </span>"
fi

mousbat="󰍽 $(mousebattery)"

# Volume
volume="<span foreground='$col_ac1'>$pl_left</span><span background='$col_ac1' foreground='$col_bg'>  $(pactl get-sink-volume 0 | grep % | awk '{print $5}')</span><span foreground='$col_ac1'>$pl_right</span>"

echo "$mousbat  $memory"
