local wezterm = require 'wezterm'
local act = wezterm.action

local config = wezterm.config_builder()

config.default_prog = { '/usr/bin/zsh' }

config.initial_rows = 50
config.initial_cols = 120

config.font = wezterm.font 'JetBrainsMono Nerd Font'
config.font_size = 10
config.color_scheme = 'Modus Vivendi Tinted (Gogh)'

config.hide_tab_bar_if_only_one_tab = true

config.keys = {
    { key = 'r', mods = 'CTRL|SHIFT', action = act.ActivateKeyTable { name = 'resize_pane', one_shot = false, }, },
    { key = 't', mods = 'CTRL|ALT',   action = act.SpawnTab 'CurrentPaneDomain' },
    { key = 'l', mods = 'CTRL|ALT',   action = act.SplitHorizontal { domain = 'CurrentPaneDomain' } },
    { key = 'j', mods = 'CTRL|ALT',   action = act.SplitVertical { domain = 'CurrentPaneDomain' } },
    { key = 'h', mods = 'ALT',        action = act.ActivatePaneDirection 'Left' },
    { key = 'j', mods = 'ALT',        action = act.ActivatePaneDirection 'Down' },
    { key = 'k', mods = 'ALT',        action = act.ActivatePaneDirection 'Up' },
    { key = 'l', mods = 'ALT',        action = act.ActivatePaneDirection 'Right' },
    { key = 'h', mods = 'ALT|SHIFT',  action = act.ActivateTabRelative(-1) },
    { key = 'l', mods = 'ALT|SHIFT',  action = act.ActivateTabRelative(1) },
}

config.key_tables = {
    resize_pane = {
        { key = 'LeftArrow',  action = act.AdjustPaneSize { 'Left', 5 } },
        { key = 'h',          action = act.AdjustPaneSize { 'Left', 5 } },

        { key = 'RightArrow', action = act.AdjustPaneSize { 'Right', 5 } },
        { key = 'l',          action = act.AdjustPaneSize { 'Right', 5 } },

        { key = 'UpArrow',    action = act.AdjustPaneSize { 'Up', 5 } },
        { key = 'k',          action = act.AdjustPaneSize { 'Up', 5 } },

        { key = 'DownArrow',  action = act.AdjustPaneSize { 'Down', 5 } },
        { key = 'j',          action = act.AdjustPaneSize { 'Down', 5 } },

        { key = 'Escape',     action = 'PopKeyTable' },
    },
}

return config
