# Color Variables>
RED="\033[0;31m"
GRE='\033[0;32m'
YEL="\033[0;33m"
BLU='\033[0;34m'
MAG="\033[0;35m"
CYA="\033[0;36m"
BWHT="\033[0;37m"
BRED="\033[1;31m"
BGRE='\033[1;32m'
BYEL="\033[1;33m"
BBLU='\033[1;34m'
BMAG="\033[1;35m"
BCYA="\033[1;36m"
BWHT="\033[1;37m"
NC='\033[0m'

# If you come from bash you might have to change your $PATH.
# export PATH=$HOME/bin:$HOME/.local/bin:/usr/local/bin:$PATH

# Path to your Oh My Zsh installation.
export ZSH="$HOME/.oh-my-zsh"

# Set name of the theme to load --- if set to "random", it will
# load a random theme each time Oh My Zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="agnoster" # set by `omz`

plugins=(
    git
    zsh-syntax-highlighting
    zsh-completions
    zsh-autosuggestions
    zsh-abbr
)

source $ZSH/oh-my-zsh.sh

# Sourcing other files
source "$HOME/.zsh_aliases"

# Load completions
autoload -Uz compinit && compinit

# History
HISTSIZE=5000
HISTFILE=~/.zsh_history
SAVEHIST=$HISTSIZE
HISTDUP=erase
setopt appendhistory
setopt sharehistory
setopt hist_ignore_space
setopt hist_ignore_all_dups
setopt hist_save_no_dups
setopt hist_ignore_dups
setopt hist_find_no_dups

# Completion styling
zstyle ':completion:*' matcher-list 'm:{a-z}={A-Za-z}'
zstyle ':completion:*' list-colors "${(s.:.)LS_COLORS}"
zstyle ':completion:*' menu no
zstyle ':fzf-tab:complete:cd:*' fzf-preview 'ls --color $realpath'
zstyle ':fzf-tab:complete:__zoxide_z:*' fzf-preview 'ls --color $realpath'

# Shell integrations
eval "$(fzf --zsh)"
eval "$(zoxide init --cmd cd zsh)"

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# SSH Agent?
SSH_ENV="$HOME/.ssh/agent-environment"

function start_agent {
    echo "Initialising new SSH agent..."
    /usr/bin/ssh-agent | sed 's/^echo/#echo/' >"$SSH_ENV"
    echo succeeded
    chmod 600 "$SSH_ENV"
    . "$SSH_ENV" >/dev/null
    /usr/bin/ssh-add;
}

# Source SSH settings, if applicable

if [ -f "$SSH_ENV" ]; then
    . "$SSH_ENV" >/dev/null
    #ps $SSH_AGENT_PID doesn't work under Cygwin
    ps -ef | grep $SSH_AGENT_PID | grep ssh-agent$ >/dev/null || {
        start_agent
    }
else
    start_agent
fi

if [ -s "$HOME/.var/log/urlwatch.log" ]; then
	URLWATCH="${BRED}Check Urlwatch Log${NC}"
else
	URLWATCH="${BGRE}No Updates        ${NC}"
fi

echo -e ""
echo -e "╭ ${BMAG}Welcome${NC} ──────────────────────┬───────────────────────────────────────────────╮"
echo -e "│                               │                                               │"
echo -e "│  ${BLU} ${NC}Commands:                  │  ${BLU} ${NC}Zellij:                                    │"
echo -e "│  ${BGRE}mountall${NC}    Mount Shares    │  ${BGRE}zellij_start${NC}        Start Session Selector  │"
echo -e "│  ${BGRE}unmountall${NC}  Unmount Shares  │  ${BGRE}zellij_sessionizer${NC}  Start Customer Session  │" 
echo -e "│  ${BGRE}globalgit${NC}   Check all Repos │  ${BGRE}zellij_update${NC}       Update ZelliJ           │"
echo -e "│  ${BGRE}nvminit${NC}     Enable nvm      │                                               │"
echo -e "│                               │                                               │"
echo -e "├───────────────────────────────┤                                               │"
echo -e "│                               │                                               │"
echo -e "│  ${BLU}󰏕 ${NC}Updates:                   │                                               │"
echo -e "│  ${BGRE}fullupdate${NC}  Update System   │                                               │"
echo -e "│  ${BGRE}nvimup${NC}      Update Neovim   │                                               │"
echo -e "│  ${BGRE}ffup${NC}        Update Firefox  │                                               │"
echo -e "│                               │                                               │"
echo -e "├───────────────────────────────┤                                               │"
echo -e "│                               │                                               │"
echo -e "│  ${BLU}󰾔 ${NC}UrlWatch:                  │                                               │"
echo -e "│  ${URLWATCH}           │                                               │"
echo -e "│                               │                                               │"
echo -e "╰───────────────────────────────┴───────────────────────────────────────────────╯"
echo -e ""


# FZF Commands
source "$HOME/.zsh_functions"
