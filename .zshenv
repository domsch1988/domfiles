# FZF Path
export PATH="$HOME/.fzf/bin:$PATH"
# FZF Defaults
export FZF_DEFAULT_OPTS='--multi --no-height --extended'

# User bin Paths
export PATH="$HOME/.local/bin:$PATH"

# Go Binary
export PATH="/usr/local/go/bin:$PATH"

# Cargo Binaries
export PATH="$HOME/.cargo/bin:$PATH"

# Neovim Distribution
export NVIM_APPNAME='nvim'

# Neovim as Manpager
export MANPAGER="sh -c 'col -bx | batcat -l man -p --paging always'"

# NPM
export NVM_DIR="$HOME/.nvm"

if [[ ! "$PATH" == */home/dosa/.fzf/bin* ]]; then
  PATH="${PATH:+${PATH}:}/home/dosa/.fzf/bin"
fi

# Guix Installed Programs
export PATH="$HOME/.guix-home/profile/bin:$PATH"

# Autopull Git Repos
ELARA_ANSIBLE_AUTO_GIT_PULL=1
